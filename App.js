import React, { Component } from 'react'
import Navigation from './scr/route/navigation'
import { Provider } from 'react-redux'
import { store } from './scr/store/index'
import { Root } from "native-base";


const App =()=>{
  return(
    <Root>
     <Provider store={store}>
     <Navigation/>
     </Provider>
     </Root>
       )
}
export default App