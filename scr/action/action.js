import { _postApiCall } from '../utils/NetworkCommunication'
import { reducerType } from '../constants/reducerType'
import { Url } from '../utils/constant/Url'

export function addTodo(id) {
    return {
        type: reducerType.ADD_TODO,
        id: id
    }
}
export function addLocalStoreDataInRedux (data){
    return{
        type:reducerType.ADD_LOCAL_DATA,
        id: data
    }
}

export function LoginclearData(){
    return {
        type: reducerType.LOGOUT
    }
}

export function LogOutEvent (){
    return{
        type:reducerType.LOGOUT
    }

}
export const fetchData = (type, body) => {
    switch (type) {
        case reducerType.GET_MODULE_TYPE:
            return (dispatch) => {
                _postApiCall(Url.getModuleTypesUrl, type, null, body, dispatch)
            }

            break;

        case reducerType.GET_LOGIN_CALL:
            return (dispatch) => {
                _postApiCall(Url.getLoginUrl, type, null, body, dispatch)
            }

            break;

        case reducerType.GET_ACTIVITY_LIST:
            return (dispatch) => {
                _postApiCall(Url.getModuleWiseActivity, type, null, body, dispatch)
            }
            break;

        case reducerType.REGISTRATION:
            return (dispatch) => {
                _postApiCall(Url.getRegistrationUrl, type, null, body, dispatch)
            }
            break;
        case reducerType.GET_RESOURCE:
            return (dispatch) => {
                _postApiCall(Url.getResoucesUrl, type, null, body, dispatch)
            }
            break;
        case reducerType.GET_QUESTION_AND_VIDEO:
                return(dispatch) => {
                    _postApiCall(Url.getQuestionAndVideo, type, null, body, dispatch)
                }
                break;
        case reducerType.GET_RED_FLAG_REPORT:
                return(dispatch) => {
                    _postApiCall(Url.getRedFlagReport, type, null, body, dispatch)
                }
                break;
        case reducerType.RED_FLAG_OPTION:
                return(dispatch) => {
                    _postApiCall(Url.getRedFlagQuestionsWithOptions, type, null, body, dispatch)
                }   
                break   ;
        // case reducerType.UPDATE_PASSWORD:
        // return(dispatch) => {
        //     _postApiCall(Url.updatePassword, type, null, body, dispatch)
        // }  
        // break; 
        case reducerType.UPDATE_PROFILE:
        return(dispatch) => {
            _postApiCall(Url.updateProfile, type, null, body, dispatch)
        }  
        break; 

       
        case reducerType.GET_VIDEO_RESOURSES:
        return(dispatch) => {
            _postApiCall(Url.getVideoResources, type, null, body, dispatch)
        }

        case reducerType.GET_ARTICLE_RESOURSES:
        return(dispatch) => {
            _postApiCall(Url.getAllArticleResources, type, null, body, dispatch)
        }
        case reducerType.FORGOT_PASSWORD:
        return(dispatch) => {
            _postApiCall(Url.forgotPassword, type, null, body, dispatch)
        }
        
        break;

    }
}


