import React, { Component } from "react";
import { View, Text, Image, Alert, Dimensions, AsyncStorage, ScrollView } from "react-native";
import { DrawerHeader } from "./DrawerHeader";
import { Card } from "./SideBarCard";
const { width, height } = Dimensions.get("window");


class SideBar extends Component {
  render() {
    const { navigate, replace } = this.props.navigation;
    
    return (
      <ScrollView style={styles.parentView}>
        <DrawerHeader 
         Name= {this.props.Name}
         Email= {this.props.Email}/>
          <View style={{borderWidth:0.5,borderColor:'#d9d9d9', marginStart:10,marginEnd:10}}/>
        <Card
          color='selected'
          source={require('../../../asset/menu.png')}
          onPress={()=> replace('Dashboard')}
          title= "Dashboard"

        />
        <Card
          source={require('../../../asset/flag.png')}
          onPress={() =>navigate('RedFlagTab')}
          title="Red Flag symptoms"
        />

         {/* <Card
          source={require('../../../asset/plus.png')}
          onPress={() =>navigate('ContactProvider')}
          title="Contact Provider"
        /> */}

         <Card
          source={require('../../../asset/resourses.png')}      
          onPress={() =>navigate('Resources')}
          title="Resource"
        />

        <View
        style = {{height:130}}/>

         <Card
          source={require('../../../asset/feedback.png')}     
          onPress={() =>navigate('FeedBack')}
          title="Give us a feedback"
        />

         <Card
        source={require('../../../asset/settings.png')}
          onPress={() =>navigate('Settings')}
          title="Settings"
        />

        <Card 
           source={require('../../../asset/logout.png')}
          onPress={() =>
            Alert.alert(
              "Exit",
              "Do you want to logout?",
              [
                { text: "Cancel", style: "cancel" },
                { text: "OK", onPress: this.props.logOutPress }
              ],
              { cancelable: false }
            )
          }
          title="Logout"
        />
       
      </ScrollView>
    );
  }
  _logoutAndClearStore() {
    const { replace } = this.props.navigation;
  
     replace("Registration");
  }
}
const styles = {
  parentView: {
    flex:1,
    height:"100%",
    backgroundColor: "#fff"
  },
  DrawerImage: {
    width: "100%",
    height: 160
  },
  text: {
    fontSize: 16,
    marginLeft: 15,
    marginTop: 10
  }
};


export default SideBar;
