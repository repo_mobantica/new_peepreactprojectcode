import React, { Component } from "react";
import {TouchableOpacity , Alert, View} from 'react-native'
import { Header, Left, Body, Right, Button, Icon, Title } from "native-base";

export default class Headers extends Component {
  render() {
    const {navigate} = this.props.navigation
    return (
      <View style={{ backgroundColor: '#fff', height: 65, flexDirection:'row', paddingTop:5 }}>
        <Left>
          <TouchableOpacity 
           onPress={this.props.onOpen}>
            <Icon
              name="align-left"
              style={{ color: "#00cccc" , margin:10}}
              type="FontAwesome"
              fontSize={10}
            />
          </TouchableOpacity>
        </Left>
        <Body>
          <Title style={{ color: "#00cccc", fontWeight: 'bold', fontSize: 18,textAlign:'center' }}>DASHBOARD</Title>
        </Body>
        <Right>
          <TouchableOpacity onPress={() => Alert.alert('bell Demo')} >
              <Icon
                   name='bell'
                   type={'FontAwesome'}
                   style={{ color: '#cca300' , margin:10}}
                   fontSize={10}
                    />
                </TouchableOpacity> 
    </Right>
      </View>
    );
  }
}
