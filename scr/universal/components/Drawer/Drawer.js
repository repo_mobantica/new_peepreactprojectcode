import React, { Component } from "react";
import { Drawer, Icon } from "native-base";
import SideBar from "./SideBar";
import Headers from "./Header";

 class DrawerBar extends Component {
  closeDrawer = () => {
    this.drawer._root.close();
  };
  openDrawer = () => {
    this.drawer._root.open();
  };

  render() {
    return (
      <Drawer
        ref={ref => {
          this.drawer = ref;
        }}
        content={
          <SideBar
            Name= {this.props.name}
            Email= {this.props.email}
            navigation={this.props.navigation}
            onClose={() => this.closeDrawer()}
            logOutPress={this.props.logOutPress}
          />
        }
        onClose={() => this.closeDrawer()}
        >
        <Headers 
          navigation= {this.props.navigation}
          onOpen={() => this.openDrawer()} />
          {this.props.children}
      </Drawer>
    );
  }
}
export default DrawerBar;