import React, { Component } from "react";
import{Image} from "react-native"
import { Text, TouchableOpacity, View } from "react-native";

import Icon from "react-native-vector-icons/FontAwesome";

export const Card = props => (
  <View>
    
  <TouchableOpacity
    onPress={props.onPress}
    style={{
      flexDirection: "row",
      margin: 10,
      justifyContent:'space-around',
      alignItems:'center',
    
    }}
  >
     <Image
         style={{ alignItems: 'center', width: 25, height: 25,marginLeft:20 }}
         source={props.source}
            />
    <Text style={props.color? styles.colortText : styles.text}>{props.title}</Text>
  </TouchableOpacity>
  <View style={{borderWidth:0.5,borderColor:'#d9d9d9', marginStart:15,marginEnd:15}}/>
  </View>
);

const styles = {
  text: {
    fontSize: 16,
    justifyContent: "center",
    width:200,
    marginLeft: 40
  },
  colortText: {
    fontSize: 16,
    color:'#ffbf00',
    justifyContent: "center",
    width:200,
    marginLeft: 40
  }
};
