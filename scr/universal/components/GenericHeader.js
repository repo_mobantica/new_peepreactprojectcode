import React, { Component } from "react";
import { Header, Left, Body, Right, Icon, Title } from "native-base";
import { TouchableOpacity, View } from "react-native";

export default class GenericHeader extends Component {
  render() {
    return (
      <View style={{ backgroundColor: "#00cccc", flexDirection: 'row',height: 65, borderBottomColor:'#fff', borderBottomWidth:1, paddingTop:5}}>
        {this._renderHeaderLeftButton()}
        {this._renderHeaderTitle()}
        {this._renderHeaderRightButton()}
      </View>
    );
  }

  _renderHeaderLeftButton() {
    const { navigate, goBack } = this.props.navigation;
    return (
      <Left>
        <Icon
          onPress={() => goBack()}
          name="arrow-back"
          style={{ color: "#fff" , marginStart:10}}
          fontSize={40}
        />
      </Left>
    );
  }
  _renderHeaderTitle() {
    return (
      <Body>
        <Title
          style={{
            fontWeight: "bold",
            fontSize: 18,
            color: "#fff",
            width:'120%'
          }}
        >
          {this.props.headerTitle}
        </Title>
      </Body>
    );
  }
  _renderHeaderRightButton() {
    const { goBack, push } = this.props.navigation;
    const { navigateTo } = this.props;
    return (
      <Right>
        {this.props.isShow ? (
          <TouchableOpacity onPress={() => push(navigateTo)}>
            <Icon
              name="add"
              style={{ color: "white", marginRight: 10 }}
              fontSize={50}
            />
          </TouchableOpacity>
        ) : null}
      </Right>
    );
  }
}
