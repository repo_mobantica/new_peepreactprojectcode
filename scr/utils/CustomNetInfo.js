// import { NetInfo } from "react-native";
import NetInfo from "@react-native-community/netinfo";
// Class created to globally access the Network Information
class CustomNetInfo {
  constructor() {
    this.isConnected = true;
    this.checkInternet();
  }

  checkInternet() {
    NetInfo.isConnected
      .fetch()
      .then(isConnected => {
        this.isConnected = isConnected;
      })
      .done(() => {
        NetInfo.isConnected.addEventListener("change", this.dispatchConnected);
      });
  }

  dispatchConnected = status => {
    this.isConnected = status;
  };
}

export default new CustomNetInfo();
