import {reducerType} from '../constants/reducerType';
import {Alert } from 'react-native';
import {Toast} from "native-base";
import axios from 'axios';
import CustomNetInfo from "./CustomNetInfo";

// we always get same instance of CustomNetInfo Class
const netInfo = CustomNetInfo;


// We don't want to show more than one similar internet error alerts
let isNoInternetAlertShowing = false;
const noInternetAlert = () => {
  if (!isNoInternetAlertShowing) {
    isNoInternetAlertShowing = true;
    return Alert.alert(
      'Alert',
      'No internet connection',
      [
        {
          text: "OK",
          onPress: () => {
            isNoInternetAlertShowing = false;
          }
        }
      ],
      { cancelable: false }
    );
  }
};

export const  _postApiCall = (url, type, token, body, dispatch) => {
  // if (!netInfo.isConnected) {
  //   return noInternetAlert();
  // }   
  dispatch(fetchIsLoading(reducerType.IS_LOADING, true));
  axios
    .post(`${url}`, body, 
    { headers: {
      "Content-Type": "application/json" ,
    }
    })
    .then(function (response) {
      dispatch(fetchIsLoading(reducerType.IS_LOADING, false));
      const response_data = response.data;
      dispatch(fetchDataSuccess(type, response_data))
      
    })
    .catch(function (error) {
      dispatch(fetchIsLoading(reducerType.IS_LOADING, false));
      Alert.alert(error.response.data.message)
    
    });
}

export const _getApiCall = (Url, type, token, dispatch ) => {
  // if (!netInfo.isConnected) {
  //   return noInternetAlert();
  // }
    dispatch(fetchIsLoading(reducerType.IS_LOADING, true));
    axios.get(`${Url}`, {
      headers: { "Content-Type": "application/json" ,
                 'Authorization': 'Bearer '+token} 
    })
    .then(function (response) {
      dispatch(fetchIsLoading(reducerType.IS_LOADING, false));
      const response_data = response.data;
      dispatch(fetchDataSuccess(type, response_data))
     
    })
    .catch(function (error) {
      dispatch(fetchIsLoading(reducerType.IS_LOADING, false));
      Alert.alert(error.response.data.message)
      
    });
  }

  export function fetchIsLoading(type, bool) {
    return { type: type, isLoading: bool };
  }
  
  export function fetchDataSuccess(type, data) {
    return { type: type, payload: data };
  }
