 
// const baseUrl = "http://192.168.2.23:8080/"


// const baseUrl ="http://192.168.0.25:8080/"
const baseUrl="http://peepwebservice.genieiot.in/"

export const Url = {
    getModuleTypesUrl  : baseUrl + "moduleType/getModuleTypes",
    getLoginUrl : baseUrl + 'patient/login',
    getRegistrationUrl:baseUrl+'patient/create',
    getActivityNameUrl: baseUrl + "activity/getActivityName",
    getResoucesUrl: baseUrl + 'resource/getAllResources',
    feedbackUrl: baseUrl + "feedback/addFeedBack",
    getModuleWiseActivity: baseUrl + 'activity/getModuleWiseActivity',
    getQuestionAndVideo: baseUrl + "activity/getActivityDetails",
    markAsDone: baseUrl + "video/addWatchVideo", 
    getRedFlagQuestionsWithOptions: baseUrl + "redFlagQuestions/getRedFlagSymptoms",
    addQuestionAnswer: baseUrl + "questions/addQuestionAnswer",
    updatePassword: baseUrl + "patient/updatePassword",
    updateProfile: baseUrl + "patient/updateProfile",
    addRedFlagSymptoms: baseUrl + "redFlagSymptoms/addRedFlagSymptoms",
    getVideoResources: baseUrl + "resource/getVideoResources",
    getAllArticleResources: baseUrl + "resource/getAllArticleResources",
    forgotPassword: baseUrl + "patient/forgotPassword",
    getRedFlagReport: baseUrl + "redFlagSymptoms/getRedFlagReport",
    
}