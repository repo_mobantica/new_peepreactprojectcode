import React, { Component } from "react";
import {
  View,
  TextInput,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Keyboard,
  Alert
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import GenericHeader from "../universal/components/GenericHeader";
import { Button, Text, Toast } from "native-base";
import validator from "validator";
import { Url } from "..//utils/constant/Url";
import { postApiCallWithPromise } from '../utils/PromiseApiCall'
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

class Feedback extends Component {
  state = { subject: "", feedback: "", flag: 0 };
  buttonPress(number) {
    this.setState({
      flag: number
    });
  }

  sendButton() {
    const { navigate } = this.props.navigation;
    if (
      validator.isLength(this.state.feedback, { min: 10 }) &&   this.state.flag > 0
    ) {
      //send feedback
      this._sendFeedback(
        this.state.subject,
        this.state.feedback,
        this.state.flag
      );
    } else {
      Toast.show({
        text: "Please Complete the field",
        buttonText: "Okay",
        position: "top",
        duration: 3000
      });
    }
  }

  render() {
    return (
      <KeyboardAwareScrollView
      style={{ backgroundColor: '#E7E7E7', flex:1}}
      scrollEnabled={true}
    >
        <View>
          <GenericHeader
            navigation={this.props.navigation}
            headerTitle={"Feedback"}
          />
             <Text style={[styles.nameText,{marginStart:15}]}> This application helping me with ontime information</Text>
          <View style={styles.mainView}>
            <View style={styles.starView}>
              {this._renderStarButton(1)}
              {this._renderStarButton(2)}
              {this._renderStarButton(3)}
              {this._renderStarButton(4)}
              {this._renderStarButton(5)}
            </View>
            {this._renderTopInputField()}

            {this._renderBottomButton()}
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }

  _renderTopInputField() {
    return (
      <View>
        <Text style={styles.nameText}> Anything you want to specify </Text>
        <TextInput
          onChangeText={text => {
            this.setState({ feedback: text });
          }}
          placeholder="Min of 10 characters"
          multiline={true}
          style={[
            styles.nameTextFieldStyle,
            styles.feedbackDetailsTextViewStyle
          ]}
          underlineColorAndroid="transparent"
        />
      </View>
    );
  }
  _renderBottomButton() {
    return (
      <TouchableOpacity onPress={() => this.sendButton()} style={styles.button}>
        <Text style={styles.buttonText}>Send Feedback</Text>
      </TouchableOpacity>
    );
  }
  _renderStarButton(buttonNumber) {
    return (
      <TouchableOpacity
        style={styles.starButtonStyle}
        onPress={() => this.buttonPress(buttonNumber)}
      >
        <Icon
          name="star"
          size={30}
          color={this.state.flag >= buttonNumber ? "#F39C12" : "#fff"}
        />
      </TouchableOpacity>
    );
  }

  async _sendFeedback(subject, feedbck, ratings) {
    const { goBack } = this.props.navigation;
    const body = {
    "userId":this.props.UserId,
		"description":feedbck,
		"feedBackOptionName":ratings
    };

   // const token = await getItem(ACCESS_TOKEN_KEYNAME);
    postApiCallWithPromise(Url.feedbackUrl, body)
      .then(response => {
        Alert.alert(
          "Success",
            response.data.message,
          [{ text: "OK", onPress: () => goBack() }],
          { cancelable: false }
        );
      })
      .catch(function(error) {
        reject(error);
      });
  }
}

const styles = {
  mainView: {
    padding: 30,
    backgroundColor: "#E7E7E7",
    height: "100%"
  },
  nameText: {
    fontWeight: "bold",
    fontSize: 18,
    marginTop: 20,
    marginBottom: 10
  },
  nameTextFieldStyle: {
    borderWidth: 1,
    marginTop: 3,
    padding: 15,
    fontSize: 18,
    borderColor: "gray",
    height: 120,
    borderRadius: 10
  },
  feedbackButtoonStyle: {
    marginTop: 40,
    marginLeft: 40,
    marginRight: 40,
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#fff",
    padding: 20,
    borderWidth: 1,
    backgroundColor: "#fff"
  },
  starView: {
    flexDirection: "row",
    marginRight: 25,
    marginLeft: 25
  },
  starIcon: {
    flex: 1,
    alignItems: "center"
  },
  starButtonStyle: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#E7E7E7"
  },
  feedbackDetailsTextViewStyle: { paddingTop: 15, height: 100 },
  button: {
    alignItems: "center",
    backgroundColor: "#cca300",
    borderRadius: 20,
    margin: 20,
    paddingLeft: 20,
    paddingRight: 20
  },
  buttonText: {
    fontSize: 18,
    color: "white",
    margin: 12
  }
};
const mapStateToProps = state => {

  return {
   // activities: state.GetModuleType.activities,
    UserId: state.GetModuleType.patientinfo.id,

  };

};

export default connect(mapStateToProps, null)(Feedback);
