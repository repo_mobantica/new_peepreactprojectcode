import React, { Component } from "react";
import { View, Text, TouchableOpacity, TextInput,Alert } from "react-native";
import GenericHeader from "../universal/components/GenericHeader";
import { message } from "../constants/string";
import { connect } from "react-redux";
import {reducerType } from '../constants/reducerType'
import { fetchData } from '../action/action'
import { bindActionCreators } from "redux";
import { Toast } from "native-base";
import { postApiCallWithPromise} from '../utils/PromiseApiCall'
import {Url} from '../utils/constant/Url'
import Spinner from "../universal/components/Spinner";

const ContainerWithSpinner = Spinner(View);


class ChangePassword extends Component {
    state = {
        password: '',
        reEnterPassword: '',
        oldPassword: '',
        isLoading:false
    }
    // componentDidUpdate(nextProps, nextState) {
    //      if(this.props.UpdatePassword != nextProps.UpdatePassword){
    //         Alert.alert(
    //             "Success",
    //             this.props.UpdatePassword,
    //             [
    //               {
    //                 text: "OK",
    //                 onPress: () =>this.props.navigation.goBack()
    //               }
    //             ],
    //             { cancelable: false }
    //           );
    //      }
    //   }

      verifyOldPassword = () =>{
          const {password} = this.props.loginData
          if (this.state.oldPassword === password){
              return true
          }else{
            return Toast.show({
                text: "incorrect old password",
                buttonText: "Okay",
                position: "top",
                duration: 5000
              })
          }
       }

    passwordValidation() {
        if(this.verifyOldPassword()){
        if(this.state.password &&  this.state.reEnterPassword){
        if (this.state.password == this.state.reEnterPassword) {
          return true;
        } else {
         return Toast.show({
          text: "password mismatch please check",
          buttonText: "Okay",
          position: "top",
          duration: 5000
        })
        }}
        else{
            return Toast.show({
                text: "Text field getting null",
                buttonText: "Okay",
                position: "top",
                duration: 5000
              })
        }
      }
    }

    render() {
        const loginData = this.props.loginData;
        const { navigate } = this.props.navigation
        return (
                <ContainerWithSpinner isLoading= {this.state.isLoading}
                 style={{ backgroundColor: "#fff", flex: 1 }}>
                    <GenericHeader
                        navigation={this.props.navigation}
                        headerTitle={"Change Password"}
                    />

                    {/* <Text style={styles.titleAlign}>Update Password</Text> */}
                    <View style={styles.subView}>

                    <TextInput
                            ref={input => {
                                this.password = input;
                            }}
                            returnKeyType={"next"}
                            underlineColorAndroid={"transparent"}
                            onSubmitEditing={() => {
                                this.reEnterPassword.focus();
                            }}
                            blurOnSubmit={false}
                            style={styles.textInput}
                            secureTextEntry={true}
                            placeholder={"Old Password*"}
                            onChangeText={oldPassword => this.setState({ oldPassword })}
                            value={this.state.oldPassword}
                        />

                        <TextInput
                            ref={input => {
                                this.password = input;
                            }}
                            returnKeyType={"next"}
                            onSubmitEditing={() => {
                                this.reEnterPassword.focus();
                            }}
                            blurOnSubmit={false}
                            underlineColorAndroid={"transparent"}
                            style={styles.textInput}
                            secureTextEntry={true}
                            placeholder={"Password*"}
                            onChangeText={password => this.setState({ password })}
                            value={this.state.password}
                        />

                        <TextInput
                            ref={input => {
                                this.reEnterPassword = input;
                            }}
                            style={styles.textInput}
                            underlineColorAndroid={"transparent"}
                            secureTextEntry={true}
                            placeholder={"Repeat Password*"}
                            onChangeText={reEnterPassword =>
                                this.setState({ reEnterPassword })
                            }
                            value={this.state.reEnterPassword}
                        />

                        <TouchableOpacity
                            onPress={() => this.UpdatePasswordAPI()}
                            style={styles.button}
                        >
                            <Text style={styles.buttonText}>
                                Update Password
                            </Text>
                        </TouchableOpacity>
                    </View>


                </ContainerWithSpinner>
        
        );
    }
    UpdatePasswordAPI = ()=>{
        if(this.passwordValidation()){
        const body = {
            username:this.props.loginData.emailId,
         	password:this.state.password
        }
        this.setState({isLoading:true})
        postApiCallWithPromise(Url.updatePassword, body)
        .then(response => {
            this.setState({isLoading:false})
            Alert.alert(
                             "Success",
                             response.data.message,
                             [
                                 {
                                     text: "OK",
                                     onPress: () => {this.props.navigation.goBack()}
                                    }
                             ],
                             { cancelable: false }
                         );
        })
        .catch( (error) =>{
            this.setState({isLoading:false})

          reject(error);
        });
        
         // this.props.fetchData(reducerType.UPDATE_PASSWORD, body);
    }
    }
}
const styles = {
    titleAlign: {
        marginStart: 15,
        marginTop: 15,
        marginBottom: 15,
        color: "#00cccc",
        fontWeight: "bold",
        fontSize: 25
    },
    subView: {
        padding: 10,
        borderRadius: 10,
        margin: 10,
        marginTop:20,
        elevation: 5,
        borderColor: "black",
        backgroundColor: "#f2f2f2",
        padding: 10,
        justifyContent: "space-around"
    },
    subTitle: {
        fontSize: 20,
        marginStart: 15,
        color: "#5c5c3d",
        fontWeight: "bold",
        marginBottom: 5
    },
    button: {
        alignItems: "center",
        backgroundColor: "#cca300",
        borderRadius: 20,
        margin: 20,
        paddingLeft: 20,
        paddingRight: 20
      },
      buttonText: {
        fontSize: 18,
        color: "white",
        margin: 12
      },
    
      textInput: {
        margin: 10,
        borderBottomWidth:1,
        borderColor:'gray',
        fontSize: 15,
      },
};


const mapDispatchToProps = dispatch => {
    return bindActionCreators(
      {
        fetchData: fetchData
      },
      dispatch
    );
  };
const mapStateToProps = state => {
    return {
        loginData: state.GetModuleType.patientinfo,
        emailId: state.GetModuleType.patientinfo.emailId,
        UpdatePassword: state.GetModuleType.UpdatePassword
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);
