import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  FlatList,
  AsyncStorage,
  BackHandler,
  RefreshControl
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { H1, H2, H3 } from "native-base";
import { message } from "../constants/string";
import Drawer from "../universal/components/Drawer/Drawer";

import * as ART from '@react-native-community/art';
import * as Progress from "react-native-progress";
import { fetchData, LogOutEvent } from "../action/action";
import { reducerType } from "../constants/reducerType";
import Spinner from "../universal/components/Spinner";
import Icon from "react-native-vector-icons/FontAwesome";

const ContainerWithSpinner = Spinner(View);

class Dashboard extends Component {

  constructor(props) {
    super(props)
    this.state = {
      refreshing: false
    }
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }
  componentDidMount() {
    this._didFocusAPICall = this.props.navigation.addListener("didFocus", this.homeApiCall);
  }

  componentWillUnmount() {
    // Remove the listener when you are done
    this._didFocusAPICall.remove();
  }

  handleBackButtonClick() {
    this.props.navigation.goBack(null);
    return true;
  }

  // componentDidMount() {
  //   this.homeApiCall()
  //   BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  //  }



  homeApiCall = () => {
    const body = {
      username: this.props.email,
      password: this.props.password
    };
    this.props.fetchData(reducerType.GET_MODULE_TYPE, body);
  }

  _keyExtractor = (item, index) => item.id;


  async _logoutAndClearStore() {
    const { replace } = this.props.navigation;
    await AsyncStorage.removeItem(message.SAVE_LOGIN_DATA);
    this.props.Logout()
    replace("Registration");
  }

  render() {
    const { navigate } = this.props.navigation;
    const module = this.props.moduleType
    const progressData = this.props.progressDataHomePage ? this.props.progressDataHomePage / 100 : 0
    const progress = progressData.toFixed(2)
    const testProgress = this.props.progressDataHomePage ? this.props.progressDataHomePage : 0

    console.log("PprogressDataHomePage", this.props.progressDataHomePage)

    return (
      <ContainerWithSpinner
        isLoading={this.props.isLoading}
        style={{ flex: 1, backgroundColor: "#fff",zIndex: 0 }}>
        <Drawer
          navigation={this.props.navigation}
          email={this.props.email}
          onClose={true}
          logOutPress={() => this._logoutAndClearStore()}
          name={this.props.name}
        >

          <ScrollView contentContainerStyle={styles.contentContainer}>

            <ImageBackground
              style={{ width: '100%', height: 140 }}
              source={
                require("../asset/background_half.png")
              }
            >
              <Text style={[styles.subTitle, { marginTop: 105 }]}>Welcome {this.props.name}</Text>
            </ImageBackground>
            <View
              style={{
                backgroundColor: "#00B1A7",
                flex: 1,
                alignItems: "center",
              }}
            >

              <Text style={[styles.titleAlign, { margin: 1 }]}>Let's Start</Text>
              <Text style={[styles.titleAlign, { fontSize: 30 }]}>
                with basics
              </Text>
              <View
                style={{ borderWidth: 0.5, borderColor: "#fff", width: "50%" }}
              />
              <Text style={styles.subTitle}>Your Score Card</Text>
              <Progress.Circle
                color={"#ffbf00"}
                size={70}
                progress={progress}
                textStyle={{ fontSize: 18, color: '#fff', fontWeight: '800' }}
                thickness={5}
                formatText={progress => testProgress.toFixed(0) + ' %'}
                strokeCap={"round"}
                style={{ marginTop: 15 }}
                showsText={true} />

              <Text style={styles.textDetails}>
                {message.PLEASE_GO_THROUGH_THE_BELOW}
              </Text>
              {this.props.moduleType &&
                <View>{this.cardModule("Basic Care Education", this.props.moduleType[0].id, this.props.moduleType[0].finalTotalPer)}
                  {this.cardModule("Home Care Education", this.props.moduleType[1].id, this.props.moduleType[1].finalTotalPer)}
                  {this.cardModule("Know your Risk", this.props.moduleType[2].id, this.props.moduleType[2].finalTotalPer)}
                  {/* {this.cardModule("Procedures Education", this.props.moduleType[3].id, this.props.moduleType[3].finalTotalPer)} */}
                </View>
              }
            </View>
          </ScrollView>
        </Drawer>
      </ContainerWithSpinner>
    );
  }

  cardModule = (moduleName, moduleId, percentValue) => {
    const { navigate } = this.props.navigation;
    const percent = percentValue == 0 ? 0 : percentValue / 100;

    return (
      <View style={[styles.cardView, { flexDirection: 'row' }]}>
        <Image
          style={styles.iconView}
          source={
            require("../asset/video_green.png")
          }
        />
        <TouchableOpacity
          style={[styles.touchableView, { flexDirection: 'column', width: "60%" }]}
          onPress={() =>
            navigate("ModuleDetails", { moduleName: moduleName, moduleId: moduleId })
          }
        >
          <Text style={[styles.cardTitle]}>
            {moduleName}
          </Text>
          <Progress.Bar
            style={{
              marginStart: 15,
              marginTop: 5, marginEnd: 10
            }}
            color={"#ffbf00"}
            progress={percent}
            width={170}
          />
        </TouchableOpacity>

        <Icon
          style={{ alignSelf: 'center', margin: 15 }}
          name={'angle-right'}
          color={'gray'}
          size={25}
        />



      </View>


    )
  }
}

const styles = {
  cardTitle: {
    color: "#00cccc",
    fontWeight: '500',
    textAlign: 'left',
    marginTop: 15,
    marginStart: 15,
    fontSize: 20,
    height: '50%'
  },
  parentView: {
    marginTop: 10,
    flex: 1,
    margin: 10,
    justifyContent: "space-around"
  },
  iconView: {
    height: 50,
    width: 50,
    alignItems: "center",
    marginStart: 10,
    marginTop: 20,
    marginLeft: 10,
  },
  contentContainer: {
    //paddingVertical: 20
  },
  cardView: {
    margin: 10,
    borderRadius: 10,
    height: 100,
    backgroundColor: "#fff"
  },
  touchableView: {
    //  alignItems: "center"
  },
  textDetails: {
    color: "#EEE",
    fontSize: 20,
    textAlign: "center",
    margin: 20
  },
  signView: {
    borderBottomWidth: 1,
    marginLeft: 5,
    marginRight: 5,
    borderColor: "gray"
  },
  button: {
    alignItems: "center",
    backgroundColor: "#cca300",
    borderRadius: 20,
    margin: 20,
    paddingLeft: 20,
    paddingRight: 20
  },
  buttonText: {
    fontSize: 18,
    color: "white",
    margin: 12
  },
  subTitle: {
    fontSize: 18,
    marginTop: 10,
    textAlign: "center",
    color: "#EEE"
  },
  titleAlign: {
    textAlign: "center",
    margin: 5,
    color: "#fff",
    fontSize: 40
  },
  titleFirst: {
    fontSize: 30,
    textAlign: "center",
    marginTop: 20
  }
};

const mapStateToProps = state => {
  return {

    moduleType: state.GetModuleType.moduleType,
    //  if(loginData){
    email: state.GetModuleType.patientinfo.emailId,
    name: state.GetModuleType.patientinfo.firstName,
    password: state.GetModuleType.patientinfo.password,
    progressDataHomePage: state.GetModuleType.progressDataHomePage,
    //   },
    // isLoading: state.GetModuleType.isLoading
    isLoading: false
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      fetchData: fetchData,
      Logout: LogOutEvent
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
