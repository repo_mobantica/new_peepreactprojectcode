import React, { Component } from "react";
import { View, Text, TouchableOpacity, ImageBackground } from "react-native";
import { addTodo, addLocalStoreDataInRedux } from "../action/action";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { H1, H2, H3 } from "native-base";
import { AsyncStorage } from "react-native";
import { message } from "../constants/string";
import { Image } from "react-native";
class Registration extends Component {
  componentDidMount() {
    this._retrieveData();
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem(message.SAVE_LOGIN_DATA);
      if (value !== null) {
        this.props.addLocalStoreDataInRedux(JSON.parse(value));
        this.props.navigation.replace("Dashboard");
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  render() {
    const { navigate, replace } = this.props.navigation;

    return (
      <View style={{ flex: 1, backgroundColor: "#fff" }}>
        <View style={[styles.parentView,{flex:0.8}]}>
          <Image
            style={{
              alignSelf: "center",
              width: 80,
              height: 50, 
              marginTop:20, 
            }}
            source={require("../asset/app_logo.png")}
          />
          <Text style={styles.titleAlign}>{message.PATIENT_EMPOWERMENT}</Text>
          <Text style={[styles.subTitle, { width: "70%" }]}>
            {message.HELPING_YOU_PREVENT}
          </Text>
          <TouchableOpacity
            onPress={() => navigate("RegistrationForm")}
            style={styles.button}
          >
            <Text style={styles.buttonText}>{message.REGISTRATION}</Text>
          </TouchableOpacity>

          <View style={{ flexDirection: "row" }}>
            <Text style={styles.subTitle}>
              {message.ALREADY_HAVE_AN_ACCOUNT}
            </Text>
            <TouchableOpacity
              onPress={() => navigate("Login")}
              style={styles.signView}
            >
              <Text style={styles.subTitle}>Sign In</Text>
            </TouchableOpacity>
            <Text style={styles.subTitle}>here</Text>
          </View>
        </View>
        <ImageBackground
          style={{ flex: 1 }}
          source={require("../asset/register.jpeg")}
        />
      </View>
    );
  }
}

const styles = {
  parentView: {
    flex:0.6,
    alignItems: "center",
    flex: 1,
    margin: 10,
    justifyContent: "space-between"
  },
  signView: {
    borderBottomWidth: 1,
    marginLeft: 5,
    marginRight: 5,
    borderColor: "gray"
  },
  button: {
    alignItems: "center",
    backgroundColor: "#cca300",
    borderRadius: 20, 
    paddingLeft: 20,
    paddingRight: 20
  },
  buttonText: {
    fontSize: 18,
    color: "white",
    margin: 12
  },
  subTitle: {
    fontSize: 16,
    textAlign: "center",
    color: "gray"
  },
  titleAlign: {
    textAlign: "center", 
    color: "#00cccc",
    fontWeight: "400",
    fontSize: 25,
    marginLeft:10,
    marginRight:10, 
  }, 
};

const mapStateToProps = state => {
  return {
    reduxData: state.todos.data
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      addTodo: addTodo,
      addLocalStoreDataInRedux: addLocalStoreDataInRedux
    },
    dispatch
  );
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Registration);
