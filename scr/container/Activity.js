import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  Alert
} from "react-native";
import { Toast } from "native-base";
import GenericHeader from "../universal/components/GenericHeader";
import { message } from "../constants/string";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import ArrayData from "../utils/json.json";
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";
import { fetchData } from "../action/action";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { reducerType } from "../constants/reducerType";
import { postApiCallWithPromise } from "../utils/PromiseApiCall";
import { Url } from "../utils/constant/Url";
import Icon from "react-native-vector-icons/FontAwesome";
import Spinner from "../universal/components/Spinner";
const ContainerWithSpinner = Spinner(View);

class Activity extends Component {
  state = {
    selectedValue: "",
    backColor: "#fff",
    disablrButoon: false,
    textColor: "#3399ff",
    radioValueData: [],
    uniqueData: []
  };

  componentDidMount() {
    this.getQuestionAnswerFromApi();
  }

  getQuestionAnswerFromApi = () => {
    const { params } = this.props.navigation.state;
    const body = {
      username: this.props.emailId,
      password: this.props.password,
      pageNo: "1",
      activityId: params ? params.activityId : null
    };
    this.props.fetchData(reducerType.GET_QUESTION_AND_VIDEO, body);
    //
  };

  markAsDoneApiCall(item) {
    const { params } = this.props.navigation.state;
    const body = {
      videoId: item.videoId,
      videoLink: item.videoLink,
      userId: this.props.UserId,
      activityId: params ? params.activityId : null
    };
    postApiCallWithPromise(Url.markAsDone, body)
      .then(response => {
        Alert.alert(
          "Success",
          response.data.message,
          [
            {
              text: "OK",
              onPress: () => this.getQuestionAnswerFromApi()
            }
          ],
          { cancelable: false }
        );
      })
      .catch(function(error) {
        reject(error);
      });
  }

  _keyExtractor = (item, index) => item.videoId;

  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
    const { watchQuestionCount, watchVideoCount } = this.props.questionAndVideo
      ? this.props.questionAndVideo.activityList
      : "";

    return (
      <ContainerWithSpinner
        style={{ backgroundColor: "#fff", flex: 1 }}
        isLoading={this.props.isLoading}
      >
        <GenericHeader
          navigation={this.props.navigation}
          headerTitle={"Activity"}
        />
        <KeyboardAwareScrollView
          style={{ backgroundColor: "#fff" }}
          scrollEnabled={true}
        >
          <View
            style={{
              backgroundColor: "#00cccc",
              height: 70,
              padding: 15
              //alignItems: "center"
            }}
          >
            <Text style={[styles.titleAlign]}>{params.item.activityName}</Text>
            <Text
              style={{
                fontSize: 16,
                margin: 5,
                marginBottom: 10,
                color: "#e6e6e6"
              }}
            >
              {"  "}
              <Image
                style={{ height: 15, width: 20 }}
                source={require("../asset/eye.png")}
              />
              {"  "}
              {watchVideoCount} Video {"  |  "}
              <Image
                style={{ height: 15, width: 20 }}
                source={require("../asset/question.png")}
              />
              {"  "} {watchQuestionCount}
              {"  "} Question
            </Text>
          </View>
          {this.props.questionAndVideo ? (
            <View>
              <FlatList
                data={this.props.questionAndVideo.activityList.wsVideo}
                keyExtractor={this._keyExtractor}
                renderItem={({ item }) => (
                  <View>
                    <TouchableOpacity
                      style={styles.touchableView}
                      onPress={() =>
                        navigate("PlayVideo", { videoLink: item.videoLink })
                      }
                    >
                      <Image
                        style={styles.iconView}
                        source={require("../asset/video_green.png")}
                      />
                    </TouchableOpacity>

                    <TouchableOpacity
                      disabled={item.videoFlag === "0" ? false : true}
                      onPress={() => {
                        this.markAsDoneApiCall(item);
                      }}
                      style={[
                        styles.button,
                        {
                          backgroundColor: this.state.backColor,
                          flexDirection: "row"
                        }
                      ]}
                    >
                      <Text
                        style={[
                          styles.buttonText,
                          { color: this.state.textColor }
                        ]}
                      >
                        {item.videoFlag === "0"
                          ? "Mark it as done"
                          : "Already Watched"}
                      </Text>
                      {item.videoFlag === "0" ? null : (
                        <Icon
                          style={{ alignSelf: "center" }}
                          name={"check"}
                          color={"#3399ff"}
                          size={15}
                        />
                      )}
                    </TouchableOpacity>
                  </View>
                )}
              />

              {this.props.questionAndVideo.activityList.wsQuestions.length >
              0 ? (
                <View>
                  <Text style={styles.subTitle}>
                    Let's Answer The Questions
                  </Text>
                  <View style={styles.cardView} />

                  <FlatList
                    data={this.props.questionAndVideo.activityList.wsQuestions}
                    renderItem={({ item, index }) => (
                      <View key={item.questionName}>
                        <Text style={styles.subRadioTitle}>
                          Q{index + 1}. {item.questionName}
                        </Text>
                        <RadioGroup
                          onSelect={(index, value) =>
                            this.setRadioValue(value, item)
                          }
                          selectedIndex={item.wsOption.findIndex(
                            x => x.optionId == item.answerId
                          )}
                        >
                          {item.wsOption.map((data, index) => {
                            return (
                              <RadioButton
                                value={data.optionId}
                                style={styles.radioItem}
                              >
                                <Text>{data.optionName}</Text>
                              </RadioButton>
                            );
                          })}
                        </RadioGroup>
                      </View>
                    )}
                  />
                  <TouchableOpacity
                    style={[
                      styles.button,
                      { alignSelf: "center", width: "80%" }
                    ]}
                    onPress={() => this.SubmitAPICall()}
                  >
                    <Text style={styles.buttonText}>Submit</Text>
                  </TouchableOpacity>
                </View>
              ) : null}
            </View>
          ) : null}
        </KeyboardAwareScrollView>
      </ContainerWithSpinner>
    );
  }

  SubmitAPICall() {
    if (this.state.uniqueData && this.state.uniqueData.length > 0) {
      const body = {
        addQuestionAnswer: this.state.radioValueData
      };

      postApiCallWithPromise(Url.addQuestionAnswer, body)
        .then(response => {
          Alert.alert(
            "Success",
            response.data.message,
            [
              {
                text: "OK",
                onPress: () => this.getQuestionAnswerFromApi()
              }
            ],
            { cancelable: false }
          );
        })
        .catch(function(error) {
          reject(error);
        });
    } else {
      Toast.show({
        text: "Please Complete the Question Answer First",
        buttonText: "Okay",
        position: "top",
        duration: 3000
      });
    }
  }

  setRadioValue(optionId, data) {
    const { params } = this.props.navigation.state;
    const activityId = params ? params.activityId : null;

    const array = {
      questionId: data.questionId,
      questionName: data.questionName,
      userId: this.props.UserId,
      activityId: activityId,
      answerId: optionId
    };
    if (this.state.radioValueData) {
      //let selectedObject = _.find(this.state.radioValueData, { id: optionId });
    }
    this.state.radioValueData.push(array);

    const unique = this.state.radioValueData.filter((v, i) => {
      return optionId === v.answerId ? v : null;
    });
    console.log("====================================");
    console.log(unique);
    this.state.uniqueData = unique;
    console.log("====================================");
  }
}
const styles = {
  contentContainer: {
    paddingVertical: 20
  },
  subRadioTitle: {
    color: "#5c5c3d",
    marginBottom: 5,
    padding: 10,
    marginStart: 15,
    fontSize: 15,
    marginEnd: 15
  },
  radioItem: {
    margin: 10,
    marginStart: 15,
    borderRadius: 10,
    alignSelf: "center",
    height: 40,
    width: "90%",
    borderWidth: 1,
    borderColor: "#5c5c3d"
  },
  titleAlign: {
    marginStart: 15,
    marginTop: 5,
    fontWeight: "bold",
    color: "#fff",
    fontSize: 18
  },
  subTitle: {
    fontSize: 20,
    marginStart: 15,
    color: "#5c5c3d",
    marginTop: 15
  },
  touchableView: {
    alignItems: "center"
  },
  iconView: {
    height: 120,
    padding: 10,
    width: 120,
    marginStart: 10,
    marginTop: 10,
    marginLeft: 10
  },
  cardTitle: {
    fontSize: 17,
    textAlign: "left",
    marginStart: 5,
    width: "60%"
  },
  articlesTitle: {
    fontSize: 17,
    textAlign: "left",
    marginStart: 15,
    width: "90%",
    marginBottom: 5,
    marginTop: 5
  },
  cardView: {
    margin: 5,
    borderRadius: 10,
    alignItems: "flex-start",
    backgroundColor: "#fff"
  },
  button: {
    borderColor: "#00cccc",
    borderRadius: 10,
    marginTop: 20,
    marginLeft: 15,
    backgroundColor: "#00cccc",
    marginBottom: 10,
    width: "45%",
    paddingLeft: 5,
    paddingRight: 5,
    borderWidth: 1
  },
  buttonText: {
    fontSize: 14,
    alignSelf: "center",
    color: "#fff",
    margin: 10
  }
};

const mapStateToProps = state => {
  return {
    // activities: state.GetModuleType.activities,
    emailId: state.GetModuleType.patientinfo.emailId,
    password: state.GetModuleType.patientinfo.password,
    UserId: state.GetModuleType.patientinfo.id,
    isLoading: state.GetModuleType.isLoading,
    questionAndVideo: state.GetModuleType.questionAndVideo
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      fetchData: fetchData
    },
    dispatch
  );
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Activity);
