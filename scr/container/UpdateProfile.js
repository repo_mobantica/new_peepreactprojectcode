import React, { Component } from "react";
import { View, Text, TouchableOpacity, TextInput, Alert } from "react-native";
import GenericHeader from "../universal/components/GenericHeader";
import { message } from "../constants/string";
import { connect } from "react-redux";
import { reducerType } from '../constants/reducerType'
import { fetchData,clearProfile, addLocalStoreDataInRedux } from '../action/action'
import { bindActionCreators } from "redux";
import { Toast } from "native-base";
import { postApiCallWithPromise} from '../utils/PromiseApiCall'
import {Url} from '../utils/constant/Url'
import Spinner from "../universal/components/Spinner";

const ContainerWithSpinner = Spinner(View);


class UpdateProfile extends Component {
    //constructor(props){
      //  super(props)
        // {firstName, lastName } = this.props.navigation.state.params
        state = {
            firstName:this.props.navigation.state.params.firstName,
            lastName: this.props.navigation.state.params.lastName,
        }
       
    //}
    
    fieldValidation() {
       // const {firstName, lastName } = this.props.navigation.state.params
        if ( this.state.firstName && this.state.lastName) {
            return true;
        } else {
            return Toast.show({
                text: "complete the field",
                buttonText: "Okay",
                position: "top",
                duration: 5000
            })
        }
    }

    render() {
        const loginData = this.props.loginData;
        const { navigate } = this.props.navigation
        
        return (
            <ContainerWithSpinner isLoading= {this.props.isLoading}
             style={{ backgroundColor: "#fff", flex: 1 }}>
                <GenericHeader
                    navigation={this.props.navigation}
                    headerTitle={"Profile"}
                />

                
                <View style={styles.subView}>

                    <TextInput
                        returnKeyType={"next"}
                        underlineColorAndroid={"transparent"}
                        onSubmitEditing={() => {
                            this.reEnterPassword.focus();
                        }}
                        
                        blurOnSubmit={false}
                        style={[styles.textInput,{marginTop:20}]}
                        placeholder={"First Name"}
                        onChangeText={firstName => this.setState({ firstName })}
                        value={this.state.firstName}
                    />

                    <TextInput
                        returnKeyType={"next"}
                        onSubmitEditing={() => {
                            this.reEnterPassword.focus();
                        }}
                        blurOnSubmit={false}
                        underlineColorAndroid={"transparent"}
                        style={styles.textInput}
                        placeholder={"Last Name"}
                        onChangeText={lastName => this.setState({ lastName })}
                        value={this.state.lastName}
                    />



                    <TouchableOpacity
                        onPress={() => this.UpdateProfileAPI()}
                        style={styles.button}
                    >
                        <Text style={styles.buttonText}>Update</Text>
                    </TouchableOpacity>
                </View>


            </ContainerWithSpinner>

        );
    }


    UpdateProfileAPI = () => {
        const { replace } = this.props.navigation
        if (this.fieldValidation()) {
            const {firstName, lastName } = this.props.navigation.state.params
            const body = {
                id: this.props.loginData.id,
                firstName: this.state.firstName,
                lastName: this.state.lastName
            }

            postApiCallWithPromise(Url.updateProfile, body)
            .then(response => {
                console.log("response",response)
                if(response.data.status ==1){
                this.props.addLocalStoreDataInRedux(response.data.patientinfo)
            }
                Alert.alert(
                                 "Update Profile Status",
                                 response.data.message,
                                 [
                                     {
                                         text: "OK",
                                         onPress: () => {this.props.navigation.goBack()}
                                     }
                                 ],
                                 { cancelable: false }
                             );
            })
            .catch(function (error) {
              reject(error);
            });
        }
    }
}
const styles = {
    titleAlign: {
        marginStart: 15,
        color: "#00cccc",
        marginTop: 10,
        fontWeight: "bold",
        fontSize: 20
    },
    subView: {
        padding: 10,
        borderRadius: 10,
        margin: 10,
        marginTop:20,
        elevation: 5,
        borderColor: "black",
        backgroundColor: "#f2f2f2",
        padding: 10,
        justifyContent: "space-around"
    },
    subTitle: {
        fontSize: 20,
        marginStart: 15,
        color: "#5c5c3d",
        fontWeight: "bold",
        marginBottom: 5
    },
    button: {
        alignItems: "center",
        backgroundColor: "#cca300",
        borderRadius: 20,
        margin: 20,
        paddingLeft: 20,
        marginTop:10,
        paddingRight: 20
    },
    buttonText: {
        fontSize: 18,
        color: "white",
        margin: 12
    },

    textInput: {
        margin: 10,
        borderColor: 'gray',
        borderWidth: 1,
        padding: 10,
        borderColor: 'gray',
        fontSize: 15,
        marginBottom:5
    },
};




const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            fetchData: fetchData,
            clearProfile:clearProfile,
            addLocalStoreDataInRedux: addLocalStoreDataInRedux
        },
        dispatch
    );
};
const mapStateToProps = state => {
    return {
        loginData: state.GetModuleType.patientinfo,
        emailId: state.GetModuleType.patientinfo.emailId,
        UpdateProfile: state.GetModuleType.UpdateProfile,
        firstName:state.GetModuleType.patientinfo.firstName,
        LastName: state.GetModuleType.patientinfo.lastName,
        isLoading: state.GetModuleType.isLoading

    };
 
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateProfile);
