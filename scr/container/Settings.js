import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import GenericHeader from "../universal/components/GenericHeader";
import { message } from "../constants/string";
import { connect } from "react-redux";
import { Icon } from 'native-base'
import { reducerType } from '../constants/reducerType'
import { bindActionCreators } from "redux";
import { fetchData } from '../action/action'

class Settings extends Component {
  constructor(props) {
    super(props)
    this.state = {
      refreshing: false
    }
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  handleBackButtonClick() {
    this.props.navigation.goBack(null);
    return true;
  }


  render() {
    const loginData = this.props.loginData;
    const { navigate,  } = this.props.navigation
    const {params} = this.props.navigation.state
    return (
      // loginData &&
      //  (
      <View style={{ backgroundColor: "#fff", flex: 1 }}>
        <GenericHeader
          navigation={this.props.navigation}
          headerTitle={"Settings"}
        />

        <View style={styles.subView}>
          <View style={{ justifyContent: 'space-between' }}>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
              <TouchableOpacity
                onPress={() => navigate("UpdateProfile", { firstName: loginData.firstName, lastName: loginData.lastName })}>
                <Text style={[styles.titleAlign, { fontSize: 14, fontWeight: '100' }]}>{message.EDIT_SETTING}</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text>Name</Text>
            </View>
          </View>
          <Text style={styles.hospitalDetails}>
          {loginData.firstName} {" "}
          {loginData.lastName}

          </Text>


          <Text
            style={{ marginTop: 5, flexDirection: 'row', justifyContent: 'space-between' }}
          >Email Address*</Text>
          <Text style={[styles.hospitalDetails]}>{loginData.emailId}</Text>
        </View>

        <TouchableOpacity
          onPress={() => navigate("ChangePassword")}>
          <Text style={[styles.titleAlign, { margin: 10, fontSize: 16, fontWeight: '100' }]}>Change Password</Text>
        </TouchableOpacity>
        
      </View>
      // )
    );
  }
}
const styles = {
  titleAlign: {
    marginStart: 15,
    color: "#00cccc",
    fontWeight: "bold",
    fontSize: 25
  },
  subView: {
    height: 150,
    padding: 10,
    borderRadius: 10,
    margin: 10,
    elevation: 5,
    borderColor: "black",
    backgroundColor: "#f2f2f2",
    padding: 10,
    justifyContent: "space-around"
  },
  subTitle: {
    fontSize: 20,
    marginStart: 15,
    color: "#5c5c3d",
    fontWeight: "bold",
    marginBottom: 5
  },
  hospitalDetails: {
    fontSize: 16,
    color: "#5c5c3d",
    marginBottom: 5,

  },

  subtitleContact: {
    fontSize: 20,
    marginStart: 15,
    color: "#5c5c3d",
    fontWeight: "bold",
    marginTop: 25,
    marginBottom: 5
  }
};


const mapStateToProps = state => {
  return {
    loginData: state.GetModuleType.patientinfo,
    email: state.GetModuleType.patientinfo.emailId,
    isLoading: state.GetModuleType.isLoading,
    password: state.GetModuleType.patientinfo.password
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      fetchData: fetchData
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(Settings);
