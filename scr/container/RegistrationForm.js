import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  Picker
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DatePicker from "react-native-datepicker";
import { H1, H2, Toast } from "native-base";
import { message } from "../constants/string";
import GenericHeader from "../universal/components/GenericHeader";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";
class RegistrationForm extends Component {
  state = {
    firstName: "",
    lastName: "",
    emailId: "",
    phoneNUmber: "",
    socialSecurityNumber: "",
    dateOfBirth: "",
    zipCode: "",
    country: "",
    gender: "",
    language: "", 
    stageOfChemotherapy: "",
    password: "",
    reEnterPassword: "",
    date: "",
    age:"",

  };
  onSelect(index, value) {
    this.setState({
      text: `Selected index: ${index} , value: ${value}`
    });
  }
  render() {
    const { navigate } = this.props.navigation;
    var radio_props = [
      { label: "Option 1", value: 0 },
      { label: "Option 2", value: 1 }
    ];
    return (
      <View style={{ backgroundColor: "#fff" }}>
       <GenericHeader
            navigation={this.props.navigation}
            headerTitle={"PEEP"}
          />
        <KeyboardAwareScrollView
          style={{ backgroundColor: "#fff", marginBottom:100 }}
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true}
        >
         

          <Text style={styles.titleAlign}>{message.REGISTRATION}</Text>
          {/* <Text style={styles.subTitle}>{message.BETTER_CARE_STARTS_WITH}</Text> */}
          {/* <Text style={styles.messageText}>
            {message.THANK_Q_FOR_CHOOSING_PEEP}
          </Text> */}

          <Text style={styles.subTitle}>{message.PERSONAL_INFORMATION}</Text>
          <TextInput
            style={styles.textInput}
            underlineColorAndroid="transparent"
            returnKeyType={"next"}
            onSubmitEditing={() => {
              this.lastName.focus();
            }}
            blurOnSubmit={false}
            placeholder={"First Name*"}
            onChangeText={firstName => this.setState({ firstName })}
            value={this.state.firstName}
          />
          <TextInput
            style={styles.textInput}
            ref={input => {
              this.lastName = input;
            }}
            placeholder={"Last Name*"}
            underlineColorAndroid="transparent"
            returnKeyType={"next"}
            onSubmitEditing={() => {
              this.emailId.focus();
            }}
            blurOnSubmit={false}
            onChangeText={lastName => this.setState({ lastName })}
            value={this.state.lastName}
          />
          <TextInput
            style={styles.textInput}
            underlineColorAndroid="transparent"
            ref={input => {
              this.emailId = input;
            }}
            returnKeyType={"next"}
            onSubmitEditing={() => {
              this.phoneNUmber.focus();
            }}
            blurOnSubmit={false}
            keyboardType={"email-address"}
            placeholder={"Email-address*"}
            autoCapitalize="none"
            onChangeText={emailId => this.setState({ emailId })}
            value={this.state.emailId}
          />

          <TextInput
            ref={input => {
              this.phoneNUmber = input;
            }}
            returnKeyType={"next"}
            underlineColorAndroid="transparent"
            onSubmitEditing={() => {
              this.socialSecurityNumber.focus();
            }}
            blurOnSubmit={false}
            style={styles.textInput}
            keyboardType={"numeric"}
            placeholder={"Phone Number"}
            underlineColorAndroid="transparent"
            maxLength={10}
            onChangeText={phoneNUmber => this.setState({ phoneNUmber })}
            value={this.state.phoneNUmber}
          />
          <TextInput
            ref={input => {
              this.socialSecurityNumber = input;
            }}
            returnKeyType={"next"}
            blurOnSubmit={false}
            style={styles.textInput}
            underlineColorAndroid="transparent"
            keyboardType={"numeric"}
            placeholder={"Social Security Number"}
            onChangeText={socialSecurityNumber =>
              this.setState({ socialSecurityNumber })
            }
            value={this.state.socialSecurityNumber}
          />
          <Text style={styles.subRadioTitle}>Age</Text>
          <RadioGroup
            onSelect={(index, value) => this.setState({ age: value })}
            style={styles.radio}
          >
            {this.radio("0-17", "0-17")}
            {this.radio("18-64", "18-64")}
            {this.radio(">=64", ">=64")}
          </RadioGroup>

          
          <View style={styles.pickerView}>
            <Picker
              selectedValue={this.state.gender}
              mode={"dropdown"}
              style={{ width: "90%" }}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ gender: itemValue })
              }
            >
              <Picker.Item label="Gender" value="Gender" />
              <Picker.Item label="Female" value="Female" />
              <Picker.Item label="Male" value="Male" />
              <Picker.Item
                label="Transgender Female"
                value="Transgender Female"
              />
              <Picker.Item label="Transgender Male" value="Transgender Male" />
              <Picker.Item
                label="Gender Variant/Non-Conforming"
                value="Gender Variant/Non-Conforming"
              />
              <Picker.Item
                label="Prefer Not to Answer"
                value="Prefer Not to Answer"
              />
            </Picker>
          </View>
           
          <View>
            

            <TextInput
              ref={input => {
                this.password = input;
              }}
              returnKeyType={"next"}
              onSubmitEditing={() => {
                this.reEnterPassword.focus();
              }}
              blurOnSubmit={false}
              style={styles.textInput}
              secureTextEntry={true}
              placeholder={"Password*"}
              onChangeText={password => this.setState({ password })}
              value={this.state.password}
            />

            <TextInput
              ref={input => {
                this.reEnterPassword = input;
              }}
              style={styles.textInput}
              secureTextEntry={true}
              placeholder={"Repeat Password*"}
              onChangeText={reEnterPassword =>
                this.setState({ reEnterPassword })
              }
              value={this.state.reEnterPassword}
            />
          </View>
          <TouchableOpacity
            onPress={() => this.RegistrationButtonPress()}
            style={styles.button}
          >
            <Text style={styles.buttonText}>
              {message.CONTINUE_REGISTRATION}
            </Text>
          </TouchableOpacity>
        </KeyboardAwareScrollView>
      </View>
    );
  }

  passwordValidation() {
    if (this.state.password) {
      if (this.state.password == this.state.reEnterPassword) {
        return true;
      } else {
        return Toast.show({
          text: "Password mismatch Please Check",
          buttonText: "Okay",
          position: "top",
          duration: 5000
        });
      }
    } else {
      return Toast.show({
        text: "please set password",
        buttonText: "Okay",
        position: "top",
        duration: 5000
      });
    }
  }
  emailValidation = () => {
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(this.state.emailId)) {
      return true;
    } else {
      return Toast.show({
        text: "please check your email!",
        buttonText: "Okay",
        position: "top",
        duration: 5000
      });
    }
  };

  labelEmptyCheck = value => {
    if (value) return true;
    else
      return Toast.show({
        text: "Please enter necessary fields",
        buttonText: "Okay",
        position: "top",
        duration: 5000
      });
  };

  //this method will get called from ComponentGooglePlacesSearch
  RegistrationButtonPress() {
    const { navigate } = this.props.navigation;
    const {
      firstName,
      emailId,
      phoneNUmber,
      socialSecurityNumber,
      date,
      zipCode,
      country,
      gender,
      language, 
      stageOfChemotherapy,
      password,
      lastName,
      age
    } = this.state;

    if (
      this.labelEmptyCheck(firstName) &&
      this.labelEmptyCheck(lastName) &&
      this.labelEmptyCheck(emailId) &&
      this.emailValidation() &&
      this.passwordValidation()
    ) {
      const body = {
        firstName: firstName,
        lastName: lastName,
        emailId: emailId,
        phoneNUmber: phoneNUmber,
        socialSecurityNumber: socialSecurityNumber,
        gender: gender,
        password: password,
        age:age,
        language: language, 
        dateOfBirth: date,
        zipCode: zipCode,
        country: country,
        
        
      };
      navigate("HospitalInformation", { data: body });
    }
  }

  textArea = (placeholder, type, maxLength, state) => {
    return (
      <TextInput
        testID={state}
        style={styles.textInput}
        keyboardType={type}
        placeholder={placeholder}
        maxLength={maxLength}
        onChangeText={state => this.setState({ state })}
      />
    ); 
  };

  radio = (value, text) => {
    return (
      <RadioButton value={value} style={styles.radioItem}>
        <Text>{text}</Text>
      </RadioButton>
    );
  };
}

const styles = {
  pickerView: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "space-around",
    width: "100%"
  },
  button: {
    alignItems: "center",
    backgroundColor: "#cca300",
    borderRadius: 20,
    margin: 20,
    paddingLeft: 20,
    paddingRight: 20
  },
  buttonText: {
    fontSize: 16,
    color: "white",
    margin: 10
  },

  textInput: {
    padding: 10,
    marginStart: 15,
    fontSize: 15,
    marginEnd: 15,
    borderBottomColor: "gray",
    borderBottomWidth: 1
  },
  titleAlign: {
    marginStart: 15,
    marginTop: 15,
    marginBottom: 15,
    color: "#00cccc",
    fontWeight: "bold",
    fontSize: 25
  },
  subTitle: {
    fontSize: 20,
    marginStart: 15,
    color: "#5c5c3d",
    fontWeight: "bold",
    marginBottom: 5
  },
  messageText: {
    fontSize: 17,
    marginStart: 15,
    marginBottom: 25,
    marginEnd: 15
  },
  subRadioTitle: {
    color: "#5c5c3d",
    marginBottom: 5,
    marginTop: 5,
    padding: 10,
    marginStart: 15,
    fontSize: 15,
    marginEnd: 15
  },
  radioItem: {
    margin:10,
    marginStart: 15,
    borderRadius: 10,
    alignItems: "center",
    height: 40,
    width: "80%",
    borderWidth: 1,
    borderColor: "#5c5c3d"
  },
  radio: {
    flexDirection: "row"
  }
};

const mapStateToProps = state => {
  return {
    //  reduxData: state.todos.data
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      // addTodo: addTodo
    },
    dispatch
  );
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationForm);
