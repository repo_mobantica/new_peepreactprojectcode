import React, { Component } from "react";
import { View, Text, ScrollView, Image, FlatList } from "react-native";
import { message } from "../constants/string";
import { connect } from "react-redux";
import { reducerType } from "../constants/reducerType";
import { fetchData } from "../action/action";
import { bindActionCreators } from "redux";
import { Badge } from "native-base";
import Spinner from "../universal/components/Spinner";

const ContainerWithSpinner = Spinner(View);

class RedFlagReport extends Component {
  componentDidMount() {
    const body = {
      username: this.props.emailId,
      password: this.props.password
    };
    this.props.fetchData(reducerType.GET_RED_FLAG_REPORT, body);
  }

  render() {
    // const loginData = this.props.loginData;
    return (
      <ContainerWithSpinner
        isLoading={this.props.isLoading}
        style={{ backgroundColor: "#fff", height: "100%" }}
      >
        <View>
          <Text style={[styles.titleAlign, { marginTop: 15, marginStart: 15 }]}>
            {message.CHECK_PREVIOUS_LOG}
          </Text>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Image
              style={styles.iconView}
              source={require("../asset/ic_info.png")}
            />
            <Text style={styles.titleAlign}>{message.STRONGLY_ADVICE}</Text>
          </View>
          <FlatList
            data={
              this.props.getRedFlagReport
                ? this.props.getRedFlagReport.reportQuestion
                : this.props.getRedFlagReport
            }
            style={styles.scrollView}
            keyExtractor={this._keyExtractor}
            renderItem={({ item }) => (
              <View
                style={{
                  marginTop: 10,
                  borderWidth: 1,
                  borderColor: "gray",
                  paddingTop: 5
                }}
              >
                <Text style={styles.hospitalDetails}>{item.questionName}</Text>

                <ScrollView horizontal style={styles.subView}>
                  {item.arrayOfReport.map((data, i) => {
                    return (
                      <View>
                        <Text style={styles.borderText}>{data.date}</Text>

                        <Text style={styles.borderText}>
                          {" "}
                          {data.optionName}
                          {data.redFlagStatus == 1 ? (
                            <Image
                              style={styles.badgeView}
                              source={require("../asset/ic_info.png")}
                            />
                          ) : null}
                        </Text>
                      </View>
                    );
                  })}
                </ScrollView>
              </View>
            )}
          />
        </View>
      </ContainerWithSpinner>
    );
  }
}
const styles = {
  titleAlign: {
    titleAlign: "center",
    alignItems: "center",
    color: "#00cccc",
    fontWeight: "bold",
    fontSize: 14
  },
  borderText: {
    height: 50,
    width: 120,
    padding: 15,
    borderWidth: 1,
    borderColor: "gray",
    textAlign: "center"
  },
  subView: {
    flexDirection: "row",
    backgroundColor: "#fff",
    padding: 10,
  },
  subTitle: {
    fontSize: 20,
    marginStart: 10,
    color: "#5c5c3d",
    fontWeight: "bold",
    marginBottom: 5
  },
  hospitalDetails: {
    fontSize: 16,
    marginStart: 15,
    color: "#5c5c3d",
    marginBottom: 5,
    width: "80%"
  },

  subtitleContact: {
    fontSize: 20,
    marginStart: 15,
    color: "#5c5c3d",
    fontWeight: "bold",
    marginTop: 25,
    marginBottom: 5
  },
  iconView: {
    height: 30,
    width: 30,
    alignItems: "center",
    marginStart: 10
  },
  scrollView: {
  marginBottom:70
  },
  badgeView: {
    height: 10,
    width: 10,
    marginStart: 5,
    padding: 5
  }
};
const mapStateToProps = state => {
  return {
    loginData: state.GetModuleType.patientinfo,
    emailId: state.GetModuleType.patientinfo.emailId,
    password: state.GetModuleType.patientinfo.password,
    getRedFlagReport: state.GetModuleType.getRedFlagReport,
    isLoading: state.GetModuleType.isLoading
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      fetchData: fetchData
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RedFlagReport);
