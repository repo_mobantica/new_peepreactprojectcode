import React, { Component } from "react";
import { View, Text, TouchableOpacity, TextInput, Alert } from "react-native";
import GenericHeader from "../universal/components/GenericHeader";
import { message } from "../constants/string";
import { connect } from "react-redux";
import { reducerType } from '../constants/reducerType'
import { fetchData } from '../action/action'
import { bindActionCreators } from "redux";
import { Toast } from "native-base";
import Spinner from "../universal/components/Spinner";
import {Url } from '../utils/constant/Url'
import { postApiCallWithPromise} from '../utils/PromiseApiCall'

const ContainerWithSpinner = Spinner(View);

class ForgotPassword extends Component {
    state = {
        emailId: '',
        isLoading:false
    }


    emailValidation = () => {
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(this.state.emailId)) {
            return true;
        } else {
            return Toast.show({
                text: "Please enter valid email address",
                buttonText: "Okay",
                position: "top",
                duration: 5000
            });
        }
    };
    render() {
        return (
            <ContainerWithSpinner isLoading = {this.state.isLoading}
             style={{ backgroundColor: "#fff", flex: 1 }}>
                <GenericHeader
                    navigation={this.props.navigation}
                    headerTitle={"Forgot Password"}
                />

                <Text style={styles.titleAlign}>Enter your registered email address</Text>
                <View style={styles.subView}>



                    <TextInput
                        ref={input => {
                            this.emailId = input;
                        }}
                        returnKeyType={"next"}
                        blurOnSubmit={false}
                        underlineColorAndroid={"transparent"}
                        style={styles.textInput}
                        autoCapitalize='none'
                        placeholder={"email id *"}
                        keyboardType={"email-address"}
                        onChangeText={emailId => this.setState({ emailId })}
                        value={this.state.emailId}
                    />



                    <TouchableOpacity
                        onPress={() => this.sendEmailAPI()}
                        style={styles.button}
                    >
                        <Text style={styles.buttonText}>
                            Send Email
                            </Text>
                    </TouchableOpacity>
                </View>


            </ContainerWithSpinner>

        );
    }
    sendEmailAPI = () => {
        if (this.state.emailId == '') {
            return Toast.show({
                text: "Enter registered email address",
                buttonText: "Okay",
                position: "top",
                duration: 5000
            });
        } else if (this.emailValidation()) {
            const body = {
                emailId: this.state.emailId,
            };
              
              this.setState({isLoading:true})
              postApiCallWithPromise(Url.forgotPassword, body)
              .then(response => {
                  this.setState({isLoading:false})
                  Alert.alert(
                                   "Forgot Password",
                                   response.data.message,
                                   [
                                       {
                                           text: "OK",
                                           onPress: () => {null}
                                          }
                                   ],
                                   { cancelable: false }
                               );
              })
              .catch( (error) =>{
                  this.setState({isLoading:false})
      
                reject(error);
              });
            
        }
    };
}
const styles = {
    titleAlign: {
        marginStart: 15,
        marginTop: 15,
        marginBottom: 15,
        color: "#00cccc",
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 15
    },
    subView: {
        padding: 10,
        borderRadius: 10,
        margin: 10,
        elevation: 5,
        borderColor: "black",
        backgroundColor: "#f2f2f2",
        padding: 10,
        justifyContent: "space-around"
    },
    subTitle: {
        fontSize: 20,
        marginStart: 15,
        color: "#5c5c3d",
        fontWeight: "bold",
        marginBottom: 5
    },
    button: {
        alignItems: "center",
        backgroundColor: "#cca300",
        borderRadius: 20,
        margin: 10,
        paddingLeft: 20,
        paddingRight: 20
    },
    buttonText: {
        fontSize: 18,
        color: "white",
        margin: 12
    },

    textInput: {
        margin: 10,
        padding:5,
        borderBottomWidth: 1,
        borderColor: 'gray',
        fontSize: 15,
    },
};


const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            fetchData: fetchData
        },
        dispatch
    );
};
const mapStateToProps = state => {
    return { 
        ForgotPassword: state.GetModuleType.ForgotPassword,
        isLoading: state.GetModuleType.isLoading
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
